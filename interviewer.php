<?php
$page_title = "Interviewer";
$description = "Please provide the name of the company with which you are interviewing.";
include(__DIR__."/header.php");
?>

<div class='welcome'>Welcome</div>

<h1>With which company are you interviewing?</h1>

<form class="form-signin" style="max-width: 330px;" action="who.php">

  <input id="student_id" type="hidden" name="student_id" value="" />
  <input id="purpose" type="hidden" name="purpose" value="" />

  <label for="company_name" class="sr-only">Company Name</label>
  <input id="company_name" name="company_name" class="form-control" style="border-radius: 3px;" placeholder="Company Name" required="" autofocus type="text">
  <br>
  <button class="btn btn-lg btn-danger btn-block btn-checkin" type="submit"><span>Submit</span></button>
</form>

<br><br><br>
<script type="text/javascript">

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    url = url.toLowerCase(); // This is just to avoid case sensitiveness  
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

$(function(){
  $("#student_id").val(getParameterByName("student_id"));
  $("#purpose").val(getParameterByName("purpose"));
});

</script>

<?php include(__DIR__."/footer.php"); ?>
