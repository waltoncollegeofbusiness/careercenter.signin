<?php
$page_title = "Sign In";
$description = "Sign in page for students visiting the career center.";
include(__DIR__."/header.php");
?>

<h1 style="color: white; font-size: 160px; opacity: 0.3; font-weight: 400">Check In</h1>

<form class="form-signin" style="max-width: 330px;" action="options.php">
  <label for="student_id" class="sr-only">Student ID Number</label>
  <input id="student_id" name="student_id" class="form-control" style="border-radius: 3px;" placeholder="Enter Your Student ID Number" required="" autofocus type="number" min=0>
  <br>
  <button class="btn btn-lg btn-danger btn-block btn-checkin" type="submit"><span>Submit</span></button>
</form>

<br><br><br><br><br><br><br><br><br>

<?php include(__DIR__."/footer.php"); ?>
