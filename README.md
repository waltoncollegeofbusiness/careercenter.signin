# Career Center Kiosk Check-In

The "kiosk" design is for an iPad layout. This app was written in PHP & Javascript as a means for visitors to the career center to sign-in providing some information about their visit:

* University ID Number
* Why are you here?
  * If interviewing, then with which company?
* Which staff member are you meeting?

The information is posted to [this Formstack form][1], which is then automatically piped to Salesforce.

Most of the requests, requirements, and work for this project have been documented in the Kace Helpdesk system under [TICK:20722][2]

------------------
Development Team:

* Jeff Puckett : Programmer
* Drew Stephens : Designer
* Kathryn Baker-Parks : Salesforce and Formstack Administrator
* Meredith Adkins : Career Center Staff Primary Stakeholder

-------------------
## Procedural Flow
-------------------

A series of pages with forms that post to the next, each one in turn accumulating all the data passed from its predecessors.

1. index.php
  * input student ID
2. options.php
  * buttons used to set input value of reason for visit
3. interviewer.php **(skipped if not interviewing)**
  * input interviewing company name
4. who.php
  * buttons used to set input value of staff member to visit
  * AJAX submission to Formstack with JS redirect to `thanks.php`
5. thanks.php
  * `setTimeout` for 5 seconds, then automatically redirects to start over at `index.php`

------------------
## Resource Files
------------------

* header.php
* footer.php
* css/
  * signin.css
  * Bootstrap v3.3.6
* js/
  * jQuery 2.2.1 minified is included from [code.jquery.com CDN][4]
    but a copy is included here as a backup
* images/
  * career-backdrop2.jpg
  * the-career-logo.png

---------
## Notes
---------

* Because there is no CORS header set or JSONP response, users will *always* be redirected to the `thanks.php` page, even if the Formstack post was unsuccessful. Older versions of the app allowed the user to view the Formstack success/failure page, but because there was no way to redirect/refresh the kiosk to the first check-in page automatically for the next visitor, this risk was accepted.
* The form input elements were taken from [the `Formstack -> Publish -> Advanced -> Form HTML` section][3]. In particular, the `input name="_submit" value="1"` allows the form to be auto submitted.
* This page was originally written without PHP, entirely in Javascript, so the GET parameters from forms passed from page to page are still parsed on recipient pages with the JS function `getParameterByName`

--------------

> Written with [StackEdit](https://stackedit.io/).

[1]:https://www.formstack.com/admin/form/analytics/2253138
[2]:https://helpdesk.walton.uark.edu/userui/ticket.php?ID=20722
[3]:https://www.formstack.com/admin/form/link/2253138
[4]:https://code.jquery.com/jquery-2.2.1.min.js
