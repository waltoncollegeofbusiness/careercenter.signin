<?php
$page_title = "Reason for Visit";
$description = "Page for students to select why they are visiting the career center.";
include(__DIR__."/header.php");
?>

<div class='welcome'>Welcome</div>

<h1>What are you seeing us for today?</h1>

<form id="career_form" class="form-signin" action="who.php">

  <input id="student_id" type="hidden" name="student_id" value="" />
  <input id="purpose" type="hidden" name="purpose" value="" />

  <div class="row">

    <div class="col-md-6" style="padding-top: 5px;">
      <button formstack_val="Job / Internship Searching" class="btn btn-lg btn-danger btn-block btn-checkin"><span>Job/Internship Search</span></button>
      <button formstack_val="Career Path / Advising" class="btn btn-lg btn-danger btn-block btn-checkin"><span>Career Path Advising</span></button>
      <button formstack_val="Interviewing Basics" class="btn btn-lg btn-danger btn-block btn-checkin"><span>Interviewing Basics</span></button>
      <button formstack_val="Interview" class="btn btn-lg btn-danger btn-block btn-checkin"><span>Job Interview</span></button>
      <button formstack_val="Job / Offer Negotiation" class="btn btn-lg btn-danger btn-block btn-checkin"><span> Job Offer / Negotiation</span></button>
    </div> 

    <div class="col-md-6" style="padding-top: 5px;">
      <button formstack_val="Co-op" class="btn btn-lg btn-danger btn-block btn-checkin"><span>Cooperative Education</span></button>
      <button formstack_val="Resume Review" class="btn btn-lg btn-danger btn-block btn-checkin"><span>Resume Review</span></button>      
      <button formstack_val="Cover Letter Review" class="btn btn-lg btn-danger btn-block btn-checkin"><span>Cover Letter Review</span></button>  
      <button formstack_val="Bell Ringer" class="btn btn-lg btn-danger btn-block btn-checkin"><span>I Gotta Ring That Bell!</span></button>  
      <button formstack_val="Career Path / Advising" class="btn btn-lg btn-danger btn-block btn-checkin"><span>Other...</span></button>  
    </div>

  </div>

</form>
<br><br><br>
<script type="text/javascript">

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    url = url.toLowerCase(); // This is just to avoid case sensitiveness  
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

$(function(){
  $("#student_id").val(getParameterByName("student_id"));
  $("button").click(function(){

    var purpose = $(this).attr("formstack_val");
    $("#purpose").val(purpose);

    // if interviewing, then redirect to interviewer.php
    if (purpose === "Interview") {
      $("#career_form").attr("action","interviewer.php");
    }

  });
});

</script>

<?php include(__DIR__."/footer.php"); ?>
