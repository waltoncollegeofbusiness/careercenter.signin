<?php
$page_title = "Thank You";
$description = "Thank you confirmation page after checking in at the career center.";
include(__DIR__."/header.php");
?>

<div class='welcome'>Thank You!</div>

<h1>Please take a seat and we'll be with you shortly.</h1>

<script>
setTimeout(function(){
  window.location.href = 'index.php';
}, 5 * 1000);
</script>

<?php include(__DIR__."/footer.php"); ?>
