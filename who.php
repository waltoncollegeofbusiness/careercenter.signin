<?php
$page_title = "Advisors";
$description = "Page for students to select who they are visiting at the career center.";
include(__DIR__."/header.php");
?>

<div class='welcome'>Also...</div>

<h1>Can you tell us WHO you're here to see?</h1>

<form id="career_form" class="form-signin" onsubmit="return false">

  <input name="form" value="2253138" type="hidden" />
  <input name="_submit" value="1" type="hidden" />
  <input name="viewkey" type="hidden" value="LmPH5j6jJR" />
  <input id="student_id" type="hidden" name="field39311140" />
  <input id="purpose" type="hidden" name="field39311178[]" />
  <input id="company_name" type="hidden" name="field42382841" />
  <input id='advisor' name="field39312503[]" type="hidden" />
  <input id='datetime_day' name="field39311372D" type="hidden" />
  <input id='datetime_month' name="field39311372M" type="hidden" />
  <input id='datetime_year' name="field39311372Y" type="hidden" />

  <div class="row">

    <div class="col-md-6 col-md-push-3" style="padding-top: 5px;">
      <button formstack_val="005310000070zhK" class="btn btn-lg btn-danger btn-block btn-checkin"><span>Renee Clay</span></button>
      <button formstack_val="005i0000005TrZY" class="btn btn-lg btn-danger btn-block btn-checkin"><span>Meredith Adkins</span></button>
      <button formstack_val="005310000070zh5" class="btn btn-lg btn-danger btn-block btn-checkin"><span>Trisha DuCote</span></button>
      <button formstack_val="005310000070uX5" class="btn btn-lg btn-danger btn-block btn-checkin"><span>Melissa Tatman</span></button>
      <button formstack_val="005310000070zh5" class="btn btn-lg btn-danger btn-block btn-checkin"><span> I'm not sure.</span></button>
    </div> 

  </div>

</form>

<script type="text/javascript">

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    url = url.toLowerCase(); // This is just to avoid case sensitiveness  
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function get_date_day(){
  var today = new Date();
  var dd = today.getDate();

  if(dd<10) {
      dd='0'+dd
  } 

  return dd;
}

function get_date_month(){
  var today = new Date();
  var mm = today.getMonth()+1; //January is 0!

  if(mm<10) {
      mm='0'+mm
  } 

  return mm;
}

function get_date_year(){
  var today = new Date();

  return today.getFullYear();
}

$(function(){
  $("#student_id").val(getParameterByName("student_id"));
  $("#purpose").val(getParameterByName("purpose"));
  var comp_name = getParameterByName("company_name");
  if (comp_name !== ''){
    $("#company_name").val(comp_name);
  }
  $("button").click(function(){
    $("#advisor").val($(this).attr("formstack_val"));
    $("#datetime_day").val(get_date_day());
    $("#datetime_month").val(get_date_month());
    $("#datetime_year").val(get_date_year());
    $.post( "https://www.formstack.com/forms/index.php", $("#career_form").serialize() )
      .always(function( data ) {
        window.location = "thanks.php";
      });
  });
});

</script>
<?php include(__DIR__."/footer.php"); ?>
